#include"duck.h"
#include"event.h"
#include "display.h"

using namespace std;

void
initDuck(duck &c, game &jeu)
{

    c.lecture.w=c.lecture_dead.w=LARGEUR_HITBOX;
    c.lecture.h=c.lecture_dead.h=HAUTEUR_HITBOX;
    c.lecture_dead.x=456;

    c.disappear=false;

    c.lectureMiniScores.x=8;
    c.lectureMiniScores.h=14;
    c.lectureMiniScores.w=28;

    c.lecture_fall.x=538;
    c.lecture_fall.w=36;
    c.lecture_fall.h=62;

    c.fallen=false;
    c.color= rand()%3;
    c.laps=100;
    c.fall_pos=0;
    if (c.color==0){

        c.lecture.y=225;
        c.lectureMiniScores.y=42;
        c.points=POINTS_BLACK;
        c.speed=1*jeu.niveau;
        c.lecture_dead.y=250;
        c.lecture_fall.y=245;
    }
    if (c.color==1){

        c.lecture.y=310;
        c.lectureMiniScores.y=25;
        c.points=POINTS_RED;
        c.speed=2*jeu.niveau;
        c.lecture_dead.y=330;
        c.lecture_fall.y=327;
    }

    if (c.color==2){

        c.lecture.y=400;
        c.lectureMiniScores.y=10;
        c.points=POINTS_BLUE;
        c.speed=3*jeu.niveau;
        c.lecture_dead.y=417;
        c.lecture_fall.y=411;

    }
    c.direction_x=((rand()%2)+1)*2-3;
    c.direction_y=((rand()%2)+1)*2-3;

    c.posX=rand()%(SCREEN_WIDTH-LARGEUR_HITBOX);
    c.posY=rand()%(460-HAUTEUR_HITBOX);
    c.alive=true;
    c.fled=false;

}


void
moveDucks(duck &c)
{

    SDL_Rect tmp;

    c.posX+=c.speed*c.direction_x;//on deplace le canard tant en abscisse qu'en ordonnee
    c.posY+=c.speed*c.direction_y;

    tmp.x=c.posX;
    tmp.y=c.posY;
    tmp.h=HAUTEUR_HITBOX;
    tmp.w=LARGEUR_HITBOX;

    // Correction Mouvement Horizontal

    if(!c.fled && ((tmp.x+tmp.w>SCREEN_WIDTH) || (tmp.x<0) || (rand()%100==34)))//on fait repartir le canard dans l'autre sens
    {
        c.direction_x*=-1;

    }

    // Correction Mouvement Vertical

    if(!c.fled && ((tmp.y+tmp.h > SCREEN_HEIGHT-292) || (tmp.y<0) || (rand()%100==34)))
    {
        c.direction_y*=-1;
    }

}

void flee(duck &c)
{
    if (c.alive)
    {
        c.direction_y=-12;
        c.fled=true;
    }
    if (c.posY<=0)
    {
        c.disappear=true;
    }
}

void
showDuck(duck &c, SDL_Surface *duck_img, SDL_Surface *s)
{
    c.counter=(c.counter+1)%3;
    switch(c.counter)
    {
    case 0:
        c.lecture.x=18;
        break;
    case 1:
        c.lecture.x=88;
        break;
    case 2:
        c.lecture.x=152;
        break;

    }

    applySurface(c.posX,c.posY,duck_img,s,&c.lecture);

}

void duckFall(duck &c, SDL_Surface *characters_img, SDL_Surface *miniScores_img, SDL_Surface *screen)
{

    if (c.laps<10)
    {
        applySurface(c.posX, c.posY,characters_img,screen,&c.lecture_dead);
        applySurface(c.posX+c.lecture.w,c.posY,miniScores_img,screen,&c.lectureMiniScores);
        c.laps ++;
    }

    else if (c.posY < HAUTEUR_CIEL)
    {
        c.posY +=10;
        c.fall_pos++;
        if (c.fall_pos%2==0)
            c.lecture_fall.x=538;
        else
            c.lecture_fall.x=586;
        applySurface(c.posX,c.posY,characters_img,screen,&c.lecture_fall);
    }
    else if (c.posY >=HAUTEUR_CIEL){
        c.fallen=true;
        c.disappear=true;
    }


}
