#include"display.h"
#include "event.h"

using namespace std;
/****************** Nom de la fonction **********************
* applySurface                                             *
******************** Auteur , Dates *************************
* Professeurs IUT Bordeaux 1                               *
********************* Description ***************************
*c'est le copier-coller d'une surface sur une autre        *
*********************** EntrÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©es *****************************
* on colle le rectangle "clip" de "source"
* sur "destination" ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢  partir de "x,y"        *
*********************** Sorties *****************************
 parametre de sortie : SDL_Surface contenant l'image.           *
************************************************************/


void
applySurface(int x, int y, SDL_Surface* source,
             SDL_Surface* destination, SDL_Rect* clip)
{
    SDL_Rect offset;
    offset.x = x;
    offset.y = y;
    SDL_BlitSurface( source, clip, destination, &offset );
}


SDL_Surface *
load_image( string filename )
{
    //Temporary storage for the image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;
    //Load the image
    loadedImage = IMG_Load( filename.c_str()) ;
    //If nothing went wrong in loading the image
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old image
        SDL_FreeSurface( loadedImage );
    }
    //Return the optimized image
    return optimizedImage;
}



// -- loadImageWithColorKey ---------------------
// chargement d'une image
// * parametres entrees :
// - "filename" : nom de l'image
// - "r,g,b"    : couleur de la ColorKey; cette
//   couleur devient transparente !
// * parametre de sortie : SDL_Surface contenant
//   l'image.
// ----------------------------------------------
SDL_Surface *
loadImageWithColorKey(string filename, int r, int g, int b)
{
    //The image that's loaded
    SDL_Surface* loadedImage = NULL;

    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = IMG_Load( filename.c_str() );

    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );

        //Free the old image
        SDL_FreeSurface( loadedImage );

        //If the image was optimized just fine
        if( optimizedImage != NULL )
        {
            //Map the color key
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, r, g, b );

            //Set all pixels of color R 0, G 0xFF, B 0xFF to be transparent
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }
    //Return the optimized image
    return optimizedImage;
}

/****************** Nom de la fonction **********************
* showMessageScreen                                            *
******************** Auteur , Dates *************************
* Professeurs IUT Bordeaux 1  *
********************* Description ***************************
* permet d'afficher un message a l'ecran        *
*********************** EntrÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©es *****************************
*on prend en entree une chaine de caracteres , sa position,
* sa taille de police, sa couleur, le tout a appliquer
*  sur l'ecran passe en reference                          *
***************************************************************/

void
showMessageScreen(string message,int x,int y,
                  TTF_Font *font,int fontSize,SDL_Color textColor,SDL_Surface* &screen)
{
    string mot="";
    string space=" ";
    int i=0,j;
    SDL_Surface *mes=NULL;

    j = message.find(space);
    while( j != string::npos )
    {
        mot=message.substr(i,j-i);
        if(mot != "")
        {
            mes=TTF_RenderText_Solid(font,mot.c_str(),textColor);
            applySurface(x,y,mes,screen,NULL);
            x+=mes->w;
            SDL_FreeSurface(mes);
        }
        x+=fontSize;
        i=j+1;
        j = message.find(space,i);
    }

    mot=message.substr(i);
    mes=TTF_RenderText_Solid(font,mot.c_str(),textColor);
    applySurface(x,y,mes,screen,NULL);
    SDL_FreeSurface(mes);
}

void
initDog(dog &d)
{

    d.lecture.w=DOG_WIDTH;
    d.lecture.h=DOG_HEIGHT;
    d.lecture.x=5;
    d.lecture.y=5;

    d.lecture_session.h=78;

    d.lecture_session.y=130;

    d.posXEndSession=350;
    d.posYEndSession=HAUTEUR_CIEL-15;
    d.posX=0;
    d.posY=500;
}

void
showDog(dog &d, SDL_Surface* dog_img, SDL_Surface* &screen, bool &dog_display, TTF_Font *fonts, int fontSize, int niveau)
{
    dog_display=true;
    if(dog_display){
        switch(d.counter)
        {


        case 27:
            d.posX=280;
            d.posY=490;
            d.lecture.x=5;
            d.lecture.y=120;

            break;
        case 28:
            SDL_Delay(400);
            d.posX=300;
            d.posY=450;
            d.lecture.x=125;
            d.lecture.y=120;
            d.lecture.w=70;
            break;
        case 29:

            d.posX=340;
            d.posY=380;
            d.lecture.x=210;
            d.lecture.y=120;
            dog_display=false;

            break;

        default://le chien va faire 26 pas
            d.posX+=10;
            d.lecture.x=(d.lecture.x+114)%456;//son rectangle de lecture en x allant de 5 a 455 et les rectangles etant espaces de 114 pixels
            break;
        }

        applySurface(d.posX,d.posY,dog_img,screen,&d.lecture);
        SDL_Delay(40);
        d.counter=(d.counter+1)%30;



    }
    if(!dog_display){
        ostringstream mssg1;
        mssg1.flush();
        mssg1.str("");
        mssg1 <<  niveau;

        showMessageScreen("niveau",400,200,fonts,fontSize,{0,0,0},screen);

        showMessageScreen(mssg1.str(),500,200,fonts,fontSize,{0,0,0},screen);
        SDL_Flip(screen);
        SDL_Delay(1000);
        initDog(d);


    }
}

void afficherScores(int scores, TTF_Font *fonts, int fontSize, SDL_Surface* &screen)
{
    ostringstream messcores;
    messcores.flush();
    messcores.str("");
    messcores <<  scores;

    showMessageScreen(messcores.str(),SCORES_X,SCORES_Y,fonts,fontSize,{255,255,255},screen);
}

void
showSight(SDL_Surface *sight, SDL_Surface *s, int x, int y)
{
    //les coordonnees de la souris
    applySurface(x-15 //qu'on centre en fontion de la taille de 30x30 du pointeur
                 ,y-15,sight,s,NULL);
}

void dog_session(dog &chien, game &jeu, SDL_Surface* dog_img, SDL_Surface* &screen)
{

    if (jeu.laps_anim_dog<80)
        {
        if (jeu.laps_anim_dog<40)
            chien.posYEndSession --;
        else
            chien.posYEndSession ++;
        applySurface(chien.posXEndSession, chien.posYEndSession,dog_img,screen, &chien.lecture_session);
        jeu.laps_anim_dog ++;
        }

}
