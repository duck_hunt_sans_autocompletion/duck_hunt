#ifndef MENU_H
#define MENU_H


#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>


struct struct_menu {
    int menu_fleche_x;
    int menu_fleche_y;
    int menu__font_play_x;
    int menu__font_play_y;
    int menu__font_quit_x;
    int menu__font_quit_y;

};


struct vie;
struct struct_cartridge;
struct game;

void initMenu (struct_menu &m);

void chargeMenu(bool &quit, bool &menu, bool &partie, bool &displayDog, struct_menu &m, int fontSize, SDL_Surface* menu_img,SDL_Surface* screen, TTF_Font *fonts,  SDL_Color bleu, vie& life, struct_cartridge &car, game &jeu);
#endif // MENU_H


