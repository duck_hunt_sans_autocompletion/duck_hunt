#ifndef ESSAI_H
#define ESSAI_H

#include "event.h"


const int NB_CANARD_TO_WIN = 6;
const int NB_CANARD_PAR_NIVEAU = 10;
const int NB_CANARD_PAR_SESSION = 2;
const int HAUTEUR_CIEL=490;

const int LARGEUR_HITBOX= 58;
const int HAUTEUR_HITBOX=63;

const int POINTS_BLACK=500;
const int POINTS_RED=1000;
const int POINTS_BLUE=1500;



struct duck
{
    int color;    //0 noir 1 rouge et 2 bleu
    int speed; //vitesse du canard (pour gÃƒÆ’Ã‚Â©rer son dÃƒÆ’Ã‚Â©placement)
    int direction_x;
    int direction_y;
    int posX;       //position x sur le champ de tir
    int posY;       //position y sur le champ de tir
    int points;
    int counter;
    int laps;
    int fall_pos;
    bool alive;
    bool fallen;
    bool fled;
    bool disappear;
    SDL_Rect lecture;
    SDL_Rect lecture_dead;
    SDL_Rect lectureMiniScores;
    SDL_Rect lecture_fall;

};


/**********************************************
 FONCTIONS D' AFFICHAGE
 **********************************************/

void
showDuck(duck &c,SDL_Surface *duck_img,  SDL_Surface *s);

struct game;
void initDuck(duck &c, game &jeu);

void duckFall(duck &c, SDL_Surface *characters_img, SDL_Surface *miniScores_img, SDL_Surface *screen);

void flee(duck &c);

/**********************************************
 FONCTIONS DE CHANGEMENTS
 **********************************************/

void
moveDucks(duck &c);

#endif // ESSAI_H

