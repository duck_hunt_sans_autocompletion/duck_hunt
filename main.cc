/*    Copyright (C) 2015 BEN KHALED Manel and GARCIA Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/


#include <iostream>
#include <cstdlib>
#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include <fstream>
#include "display.h"
#include "duck.h"
#include "event.h"
#include "menu.h"

using namespace std;


int main(int argc, char* argv[])
{
    bool quit=false;
    bool menu=true;
    bool partie=false;
    bool dog_display=false;


    int x, y;
    SDL_Event event;
    game jeu;
    jeu.scores=0;
    jeu.niveau=1;
    struct_menu m;
    vie life;
    struct_cartridge car;


    srand(time(NULL));

    SDL_Init(SDL_INIT_EVERYTHING);

    dog d;
    initDog(d);

    SDL_ShowCursor(0);

    SDL_Surface *screen=SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,
                                         SCREEN_BPP,SDL_SWSURFACE);

    SDL_Surface *background = load_image("sprites/backGame.png");
    SDL_Surface *background2 = loadImageWithColorKey("sprites/backGameBlit.png",0,0,0);


    SDL_Surface *menu_img =load_image("sprites/menu.png");

    SDL_Surface *sight=loadImageWithColorKey("sprites/viseur.png",0,0,0);

    SDL_Surface *life_img = load_image("sprites/hit.png");

    SDL_Surface *cartridge_img = loadImageWithColorKey("sprites/shot.png",255,255,255);

    SDL_Surface *characters_img=loadImageWithColorKey("sprites/duck.png",228,255,0);

    SDL_Surface *miniScores_img=loadImageWithColorKey("sprites/points.png",0,0,0);

    duck c1;
    initDuck(c1,jeu);

    duck c2;

    initDuck(c2,jeu);
    c1.alive=true;
    c2.alive=true;
    TTF_Init();
    int fontSize=30;
    TTF_Font *fonts = TTF_OpenFont("font/duck_hunt.ttf",fontSize);
    if(fonts == NULL){
        cerr << "Font non chargee" << endl;
        return 1;
    }

    SDL_Color bleu = {0,0,255};

    initMenu(m);
    initGame(jeu);


    while(!quit)
    {
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
                quit=true;

            if( event.type == SDL_MOUSEBUTTONDOWN && !dog_display && (c1.alive || c2.alive))
            {
                jeu.cartridge--;

                if(c1.alive || c2.alive)
                {
                    if (duckShot(c1,event,jeu)==true)
                    {
                        redLife(c1,jeu,life);
                        jeu.compteur ++;
                        c1.laps=0;

                    }

                    else if (duckShot(c2,event,jeu)==true)
                    {
                        redLife(c2,jeu ,life);
                        jeu.compteur ++;
                        c2.laps=0;
                    }

                }


            }

        }

        if(menu)
        {
            chargeMenu(quit,menu,partie, dog_display,m,fontSize, menu_img,screen,fonts,bleu,life,car,jeu);
        }

        if(partie)
        {

            applySurface(0,0,background,screen,NULL);


            if(dog_display)
            {
                showDog(d,characters_img,screen,dog_display,fonts,fontSize,jeu.niveau);
            }


            else
            {
                SDL_GetMouseState(&x, &y);

                if(c1.alive)
                {
                    showDuck(c1,characters_img,screen);
                    moveDucks(c1);
                }

                if(c2.alive)
                {
                    showDuck(c2,characters_img,screen);
                    moveDucks(c2);
                }
                if(!c1.alive)
                    duckFall(c1,characters_img, miniScores_img, screen);
                if(!c2.alive)
                    duckFall(c2,characters_img, miniScores_img, screen);
                if (jeu.cartridge<=0)
                {
                 flee(c1);
                 flee(c2);
                }

                if (c1.disappear && c2.disappear)
                {
                endOfSession(c1,c2,d,life,jeu);
                dog_session(d,jeu,characters_img,screen);
                newSession(jeu, c1, c2);
                }

                if(jeu.session==6 && jeu.laps_anim_dog>100)
                    endOfLevel(c1, c2,jeu,dog_display,life, partie);

                applySurface(0,0,background2,screen,NULL);
                afficherScores(jeu.scores,fonts, fontSize, screen);
                afficherShot(jeu, cartridge_img,screen,car);

                displayLife(life,life_img,screen);


                showSight(sight,screen,x, y);
            }



        }
        else if (jeu.game_over)
        {

            applySurface(0,0,background,screen,NULL);

            menu=true;

            showMessageScreen("GAME OVER",SCREEN_WIDTH/2,SCREEN_HEIGHT/2,fonts,fontSize,{0,0,0},screen);
            jeu.game_over=false;
            SDL_Flip(screen);
            SDL_Delay(3000);
        }

        SDL_Flip(screen);
        SDL_Delay(30);

    }

    TTF_CloseFont(fonts);
    TTF_Quit();
    SDL_FreeSurface(screen);
    SDL_Quit();

    return EXIT_SUCCESS;

}


//verifier code mort
//optimisier dÃƒÂ©coupage fonctionnel.
//Des constantes
