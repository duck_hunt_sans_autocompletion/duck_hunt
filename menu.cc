#include "menu.h"
#include "display.h"
#include "event.h"

using namespace std;


void initMenu (struct_menu &m)
{
    m.menu_fleche_x=0;
    m.menu_fleche_y=250;
    m.menu__font_play_x=90;
    m.menu__font_play_y=250;
    m.menu__font_quit_x=100;
    m.menu__font_quit_y=330;
}

void chargeMenu(bool &quit, bool &menu, bool &partie, bool &displayDog, struct_menu &m , int fontSize, SDL_Surface* menu_img,SDL_Surface* screen, TTF_Font *fonts,  SDL_Color bleu, vie& life, struct_cartridge &car, game &jeu)
{


    //afficherMenu
    applySurface(0,0,menu_img,screen,NULL);
    showMessageScreen("Mode Classique",m.menu__font_play_x,m.menu__font_play_y,fonts,fontSize,bleu,screen);
    showMessageScreen("Quitter",m.menu__font_quit_x,m.menu__font_quit_y,fonts,fontSize,bleu,screen);
    showMessageScreen("-->",m.menu_fleche_x,m.menu_fleche_y,fonts,fontSize,bleu,screen);

    //choixJoueur
    Uint8 *keystates = SDL_GetKeyState( NULL );

    //Si le joueur appuie sur la fleche
    if( keystates[ SDLK_DOWN ])
    {
        m.menu_fleche_y=m.menu__font_quit_y;

        m.menu__font_play_x=100;
        m.menu__font_quit_x=90;

    }
    else if ( keystates[ SDLK_UP ])
    {
        m.menu_fleche_y=m.menu__font_play_y;

        m.menu__font_quit_x=100;
        m.menu__font_play_x=90;


    }

    //Et s'il appuie sur entree.
    if (m.menu_fleche_y==m.menu__font_quit_y && keystates[SDLK_RETURN])
        quit=true;
    else if (m.menu_fleche_y==m.menu__font_play_y && keystates[SDLK_RETURN])
    {
        menu=false;
        partie=true;
        displayDog = true;
        initLife(life);
        initCartridge(car);
        initTabLecture(life);
        initGame(jeu);
    }
}

