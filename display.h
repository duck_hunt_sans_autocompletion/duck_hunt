#ifndef DISPLAY_H
#define DISPLAY_H

#include <string>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

const int SCREEN_WIDTH=750;
const int SCREEN_HEIGHT=761;
const int SCREEN_BPP=32;

const int DOG_WIDTH=112;
const int DOG_HEIGHT=100;

const int SCORES_X=570;
const int SCORES_Y=665;




struct dog
{

    int posX;
    int posY;
    int counter=0;
    int posXEndSession;
    int posYEndSession;



    SDL_Rect lecture;
    SDL_Rect lecture_session;


};

struct struct_cartridge
{
    SDL_Rect lecture_cartridge;
   int cartridge_x;
   int cartridge_y;
};

struct game;

void
showSight(SDL_Surface *sight, SDL_Surface *s, SDL_Event event);

void
applySurface(int x, int y, SDL_Surface* source,
             SDL_Surface* destination, SDL_Rect* clip);

SDL_Surface *
load_image( std::string filename );


SDL_Surface *
loadImageWithColorKey(std::string filename, int r, int g, int b);

void
showMessageScreen(std::string message,int x,int y,
                  TTF_Font *font,int fontSize,SDL_Color textColor,SDL_Surface* &screen);

void
initDog(dog &d);

void
showDog(dog &d,SDL_Surface* dog_img, SDL_Surface* &screen, bool &dog_display,TTF_Font *fonts, int fontSize, int niveau);

void afficherScores(int scores, TTF_Font *fonts, int fontSize, SDL_Surface* &screen);
void
showSight(SDL_Surface *sight, SDL_Surface *s, int x, int y);
void dog_session(dog &chien, game &jeu, SDL_Surface* dog_img, SDL_Surface* &screen);

#endif // DISPLAY_H


