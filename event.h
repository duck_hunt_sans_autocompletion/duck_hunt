#ifndef EVENT_H
#define EVENT_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <sstream>
#include "duck.h"

const int RECTANGLE_VIE=516-180;
const int TAILLE_LEC =10;
const int CARTRIDGE_MAX=3;

typedef SDL_Rect TabLectures[TAILLE_LEC];

struct game
{
    bool niveau_gagne;
    int duckleft;
    int niveau;
    int session;
    int scores;
    int compteur;
    int cartridge;
    bool game_over;
    int laps_anim_dog;

};


struct vie
{

    int posx;
    int posy;

    TabLectures tabLec;

    SDL_Rect lectureB;


    SDL_Rect lectureR;


    SDL_Rect lectureV;



};

struct duck;
struct struct_cartridge;
struct dog;

void initGame(game &jeu);
void initLife(vie & life);
void displayLife(vie &life, SDL_Surface *life_img, SDL_Surface *screen);
bool duckShot(duck &c, SDL_Event& event, game &jeu);

void initCartridge(struct_cartridge &car);
void initTabLecture(vie &life);
void redLife(duck &c, game &jeu, vie &life);

void endOfSession(duck &c1, duck &c2,dog &chien, vie &life, game &jeu);
void endOfLevel(duck &c1, duck &c2, game &jeu, bool &displayDog, vie &life, bool &partie);
void afficherShot(game &jeu, SDL_Surface* cartridge_img, SDL_Surface* &screen, struct_cartridge &car);
void newSession (game &jeu, duck &c1, duck &c2);

#endif // EVENT_H

