#include "event.h"
#include "display.h"
#include "duck.h"


using namespace std;

void initGame(game &jeu)
{
    jeu.niveau_gagne=false;

    jeu.cartridge = CARTRIDGE_MAX;
    jeu.compteur=0;
    jeu.session=1;
    jeu.duckleft=10;
    jeu.game_over=false;
    jeu.laps_anim_dog=1000;

}


void initCartridge(struct_cartridge &car)
{
    car.cartridge_x=62;
    car.cartridge_y=659;
    car.lecture_cartridge.y=2;
    car.lecture_cartridge.w=74;
    car.lecture_cartridge.h=48;

}

void initLife(vie & life)
{

    life.lectureB.x=6;
    life.lectureB.y= life.lectureR.y= life.lectureV.y=0;
    life.lectureB.h= life.lectureR.h= life.lectureV.h=33;
    life.lectureB.w= life.lectureR.w= life.lectureV.w=27;


    life.lectureR.x=30;
    life.lectureV.x=54;

    life.posy= 666;

}


void initTabLecture(vie &life)
{
    for (int i=0; i<TAILLE_LEC; i++)
        life.tabLec[i]=life.lectureB;
}

void redLife(duck &c, game &jeu, vie &life)
{
    if (!c.alive)
        life.tabLec[jeu.compteur]=life.lectureV;

}



bool duckShot(duck &c, SDL_Event& event, game &jeu)
{
    if (c.alive && (event.button.x >= c.posX) && (event.button.x <= c.posX+LARGEUR_HITBOX) && (event.button.y>=c.posY) && (event.button.y <= c.posY+HAUTEUR_HITBOX))//ce qui est mort ne saurait mourrir, John Snow
    {
        jeu.scores += c.points;
        c.alive = false;
        jeu.duckleft--;

        return true;
    }

    else
        return false;

}



void endOfSession(duck &c1, duck &c2,dog &chien, vie &life, game &jeu)
{

    if (jeu.laps_anim_dog>100)
    {//Les vies manquantes deviennent rouges
        while (jeu.compteur<NB_CANARD_PAR_SESSION*jeu.session)
        {
            life.tabLec[jeu.compteur]=life.lectureR;
            jeu.compteur ++;
        }
        //si les deux canards sont tombés à terre
        if (c1.fallen && c2.fallen)
        {
            chien.lecture_session.w=112;
            chien.lecture_session.y=130;//133
            if(c1.color==0 && c2.color==0)
                chien.lecture_session.x=582;
            if(c1.color==1 && c2.color==1)
                chien.lecture_session.x=697;
            if(c1.color==2 && c2.color==2)
                chien.lecture_session.x=1043;
            if((c1.color==0 && c2.color==1) || (c1.color==1 && c2.color==0))
                chien.lecture_session.x=1158;
            if((c1.color==1 && c2.color==2) || (c1.color==2 && c2.color==1))
                chien.lecture_session.x=926;
            if((c1.color==0 && c2.color==2) || (c1.color==2 && c2.color==0))
                chien.lecture_session.x=812;
        }
        //Si un seul canard est tombé à terre
        else if (c1.fallen)
        {

            chien.lecture_session.w=86;
            chien.lecture_session.y=130;

            if(c1.color==0)
                chien.lecture_session.x=493;
            else if(c1.color==1)
                chien.lecture_session.x=310;
            else if(c1.color==2)
                chien.lecture_session.x=401;

        }
        else if (c2.fallen)
        {
            chien.lecture_session.w=86;
            chien.lecture_session.y=130;

            if(c2.color==0)
                chien.lecture_session.x=493;
            else if(c2.color==1)
                chien.lecture_session.x=310;
            else if(c2.color==2)
                chien.lecture_session.x=401;

        }
        //Si aucun canard n'est tombé
        else
        {

            chien.lecture_session.w=58;
            chien.lecture_session.x=580;
            chien.lecture_session.y=24;
        }

        jeu.session+=1;
        jeu.laps_anim_dog=0;
    }
}


void newSession (game &jeu, duck &c1, duck &c2)
{
    if(jeu.laps_anim_dog==80)
    {

        initDuck(c1,jeu);
        initDuck(c2,jeu);
        jeu.cartridge=CARTRIDGE_MAX;
        jeu.laps_anim_dog=1000;

    }
}

void endOfLevel(duck &c1, duck &c2, game &jeu, bool &displayDog, vie &life, bool &partie)
{

    if (TAILLE_LEC-jeu.duckleft >=NB_CANARD_TO_WIN)
    {
        jeu.niveau ++;
        displayDog = true;
        initLife(life);
        jeu.cartridge=CARTRIDGE_MAX;

        jeu.duckleft=NB_CANARD_PAR_NIVEAU;

        initTabLecture(life);
        initGame(jeu);
        initDuck(c1,jeu);
        initDuck(c2,jeu);

    }
    //Si l'user n'a pas tue assez de canards
    else if (TAILLE_LEC-jeu.duckleft<NB_CANARD_TO_WIN)

    {
        partie=false;
        jeu.game_over=true;

    }

}


void afficherShot(game &jeu, SDL_Surface* cartridge_img, SDL_Surface* &screen, struct_cartridge &car)
{

    switch(jeu.cartridge)
    {
    case 0: car.lecture_cartridge.x=2;
        break;
    case 1: car.lecture_cartridge.x=78;
        break;
    case 2: car.lecture_cartridge.x=154;
        break;
    case 3: car.lecture_cartridge.x=231;
        break;
    }

    applySurface(car.cartridge_x, car.cartridge_y, cartridge_img, screen, &car.lecture_cartridge);
}

void displayLife(vie &life, SDL_Surface *life_img, SDL_Surface *screen)
{

    life.posx=195;

    for (int i=0; i<TAILLE_LEC; i++)
    {
        applySurface(life.posx, life.posy, life_img, screen , &life.tabLec[i] );
        life.posx+= RECTANGLE_VIE/11;
    }
}

//void highScores()
//{

//}
